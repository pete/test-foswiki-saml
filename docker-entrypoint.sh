#!/bin/bash

echo "...preparing Solr for Foswiki"
chown -R 8983:8983 /opt/solr/server/solr
chown -R 8983:8983 /var/www/foswiki/solr

cd /opt/solr/server/solr/configsets
[ -L foswiki_configs ] && rm foswiki_configs
ln -s /var/www/foswiki/solr/configsets/foswiki_configs/ .

cd /opt/solr/server/solr/solr_foswiki
[ -L core.properties ] && rm core.properties
ln -s /var/www/foswiki/solr/cores/foswiki/core.properties

sed -i '/SolrPlugin..Url/s/localhost/solr/' /var/www/foswiki/lib/LocalSite.cfg

#Add a passwd file - not used but cleaner
if [[ ! -e /var/www/foswiki/data/.htpasswd ]]; then
    touch /var/www/foswiki/data/.htpasswd
fi

#Add user to the admin groups
sed -i "s|value=\"\"|value=\"PeteJones\"|" /var/www/foswiki/data/Main/AdminGroup.txt
tools/configure -save -set {FeatureAccess}{Configure}="PeteJones"

#Set the Store to RCS Revision
tools/configure -save -set {Store}{Implementation}='Foswiki::Store::RcsWrap'

echo "...enabling NatSkin"
grep -q "Set SKIN = nat" /var/www/foswiki/data/Main/SitePreferences.txt || sed -i '/---++ Appearance/a\ \ \ * Set SKIN = nat' /var/www/foswiki/data/Main/SitePreferences.txt

#echo "...starting iwatch"
#iwatch -d

echo "...starting nginx+foswiki"
cd /var/www/foswiki/bin

./foswiki.fcgi -l 127.0.0.1:9000 -n 5 -d

nginx -g "daemon off;"

